package apollo.analysis;


import apollo.datamodel.StrandedFeatureSet; // from an IGB bundle?
import java.io.FileInputStream;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class PrimerBlastHtmlParserTest {

    // left over from previous code, pre-use of junit 
    // The next person who works on this should walk through the flow and capture
    // sample output and place it into the resources directory under test packages. 
    public void testParse() throws Exception {
        PrimerBlastHtmlParser parser = new PrimerBlastHtmlParser();
        FileInputStream fis = new FileInputStream("/Users/elee/blah/primer_blast_results.html");
        StrandedFeatureSet results = new StrandedFeatureSet();
        parser.parse(fis, results);
    }
    
    @Test
    public void sampleTest() {
        assertEquals(1,1);
    }   
}
